﻿## HW3

This homework is about implementation of following networks:

 - Implementing a convolutional neural network trained with **CIFAR10 dataset** without any ready functions and with the help of implemented local functions.
 - Implementing a CNN classifier on **SVHN** dataset for high accuracy
 - Implementing a fully connected, CNN and denoising Autoencoder on **MNIST**  dataset 
 - Implementing a sign language classifier using CNN
