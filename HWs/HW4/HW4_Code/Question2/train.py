from sklearn.utils import shuffle
from torch.utils.data import TensorDataset
from net import CNN
from prepare_data import prepare_data, add_background_class
import numpy as np
import torch
import torch.nn as nn
from preprocess_data import resize_all_data
from matplotlib import pyplot as plt
from transform import do_transform

num_epochs = 40
batch_size = 128
learning_rate = 0.001

Data, Label = prepare_data()
Label_numbers = np.zeros((11,))

Data, Label = do_transform(Data, Label)
Label_numbers = np.zeros((11,))
for label in Label:

    Label_numbers[label - 1] = Label_numbers[label - 1] + 1
print(Label_numbers)
New_Data, New_Label = add_background_class(Data, Label)
New_Data, New_Label = shuffle(New_Data, New_Label, random_state=1)

for i in range(len(New_Label)):
    New_Label[i] = New_Label[i] - 1

New_Data = resize_all_data(New_Data)

New_Data = np.array(New_Data) / 255
New_Label = np.array(New_Label).astype('int')

Label_numbers = np.zeros((11,))
for label in New_Label:
    Label_numbers[label] = Label_numbers[label] + 1
print(Label_numbers)

Train_Data = New_Data[0:int(0.9 * len(New_Data))]
Train_Label = New_Label[0:int(0.9 * len(New_Label))]
Validation_Data = New_Data[int(0.9 * len(New_Data)):]
Validation_Label = New_Label[int(0.9 * len(New_Label)):]

Tensor_Train_data = torch.from_numpy(np.array(Train_Data))
Tensor_Train_Label = torch.from_numpy(np.array(Train_Label))
Tensor_Validation_data = torch.from_numpy(np.array(Validation_Data))
Tensor_Validation_Label = torch.from_numpy(np.array(Validation_Label))

Tensor_Train_data = torch.reshape(Tensor_Train_data, (Tensor_Train_data.shape[0], 3, 100, 100))
Tensor_Validation_data = torch.reshape(Tensor_Validation_data, (Tensor_Validation_data.shape[0], 3, 100, 100))

train = TensorDataset(Tensor_Train_data, Tensor_Train_Label)
validation = TensorDataset(Tensor_Validation_data, Tensor_Validation_Label)

train_dl = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)

test_dl = torch.utils.data.DataLoader(validation, batch_size=batch_size, shuffle=True)

images_train, labels_train = next(iter(train_dl))
images_test, labels_test = next(iter(test_dl))

print("Shape of train inputs: ", images_train.shape, "; Shape of train labels: ", labels_train.shape)
print("Shape of test inputs: ", images_test.shape, "; Shape of test inputs: ", labels_test.shape)

# CPU or GPU

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
device

model = CNN()
print(model)

model = CNN().to(device)
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

# keeping-track-of-losses
train_losses = []
valid_losses = []
train_accuracies = []
valid_accuracies = []
for epoch in range(1, num_epochs + 1):
    # keep-track-of-training-and-validation-loss
    train_loss = 0.0
    valid_loss = 0.0
    train_accuracy = 0.0
    valid_accuracy = 0.0

    # training-the-model
    model = model.float()
    model.train()
    for data, target in train_dl:
        # move-tensors-to-GPU
        data = data.to(device)
        target = target.to(device)

        # clear-the-gradients-of-all-optimized-variables
        optimizer.zero_grad()
        # forward-pass: compute-predicted-outputs-by-passing-inputs-to-the-model
        output = model(data.float())
        _, predicted = torch.max(output.data, 1)

        predicted_array = np.array(predicted)
        sum_correct_outputs = 0
        for element_index in range(len(predicted_array)):
            if predicted_array[element_index] == target[element_index]:
                sum_correct_outputs = sum_correct_outputs + 1

        train_accuracy = train_accuracy + sum_correct_outputs
        # calculate-the-batch-loss
        # output = torch.tensor(output, dtype=torch.long, device=device)
        # target = torch.tensor(target, dtype=torch.long, device=device)

        loss = criterion(output, target.long())
        # backward-pass: compute-gradient-of-the-loss-wrt-model-parameters
        loss.backward()
        # perform-a-ingle-optimization-step (parameter-update)
        optimizer.step()
        # update-training-loss
        train_loss += loss.item() * data.size(0)

    train_accuracy = train_accuracy / Tensor_Train_data.shape[0]
    # validate-the-model
    model.eval()
    for data, target in test_dl:
        data = data.to(device)
        target = target.to(device)

        output = model(data.float())
        _, predicted = torch.max(output.data, 1)

        predicted_array = np.array(predicted)
        sum_correct_outputs = 0
        for element_index in range(len(predicted_array)):
            if predicted_array[element_index] == target[element_index]:
                sum_correct_outputs = sum_correct_outputs + 1

        valid_accuracy = valid_accuracy + sum_correct_outputs
        loss = criterion(output, target.long())

        # update-average-validation-loss
        valid_loss += loss.item() * data.size(0)

    valid_accuracy = valid_accuracy / Tensor_Validation_data.shape[0]
    # calculate-average-losses
    train_loss = train_loss / len(train_dl.sampler)
    valid_loss = valid_loss / len(test_dl.sampler)
    train_losses.append(train_loss)
    valid_losses.append(valid_loss)
    train_accuracies.append(train_accuracy)
    valid_accuracies.append(valid_accuracy)

    # print-training/validation-statistics
    print('Epoch: {} \tTraining Loss: {:.6f} \tValidation Loss: {:.6f} \tTrain Accuracy: {:.6f} \tValid Accuracy: {'
          ':.6f}'.format(epoch, train_loss, valid_loss, train_accuracy, valid_accuracy))

# test-the-model
model.eval()  # it-disables-dropout
with torch.no_grad():
    correct = 0
    total = 0
    for images, labels in test_dl:
        images = images.to(device)
        labels = labels.to(device)
        outputs = model(images.float())
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()

    print('Test Accuracy of the model: {} %'.format(100 * correct / total))

# Save
torch.save(model.state_dict(), 'model.ckpt')
plot1 = plt. figure(1)
plt.plot(train_losses, label='Training loss')
plt.plot(valid_losses, label='Validation loss')
plt.xlabel("Epochs")
plt.ylabel("Loss")
plt.legend(frameon=False)
plt.show()
plot2 = plt. figure(2)
plt.plot(train_accuracies, label='Training accuracy')
plt.plot(valid_accuracies, label='Validation accuracy')
plt.xlabel("Epochs")
plt.ylabel("Accuracy")
plt.legend(frameon=False)
plt.show()
