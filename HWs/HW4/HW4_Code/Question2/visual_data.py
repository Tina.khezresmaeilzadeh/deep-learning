import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
import numpy as np


def visualize(name):
    my_file = open('NWPU VHR-10 dataset/ground truth/' + str(name) + '.txt', 'r')
    content = my_file.read()
    content_list = content.split("\n")
    content_list = content_list[:-1]
    my_file.close()

    Labels = []
    for i in range(len(content_list)):
        txt = content_list[i].split(',')
        txt_rstipped = [x.rstrip(") ") for x in txt]
        final_txt = [int(x.strip("( ")) for x in txt_rstipped]
        Labels.append(final_txt)

    print(Labels)
    im = np.array(Image.open('NWPU VHR-10 dataset/positive image set/' + str(name) + '.jpg'), dtype=np.uint8)

    # Create figure and axes
    fig, ax = plt.subplots(1)

    # Display the image
    ax.imshow(im)

    for i in range(len(Labels)):
        rect = patches.Rectangle((Labels[i][0], Labels[i][1]), Labels[i][2] - Labels[i][0], Labels[i][3] - Labels[i][1],
                                 linewidth=2, edgecolor='g', facecolor='none')
        ax.add_patch(rect)

    for i in range(len(Labels)):
        centerx = int((Labels[i][2] + Labels[i][0]) / 2 - 10)
        centery = int((Labels[i][3] + Labels[i][1]) / 2 + 10)
        plt.text(centerx, centery, str(Labels[i][4]), color='r')
    plt.show()
    return 0
