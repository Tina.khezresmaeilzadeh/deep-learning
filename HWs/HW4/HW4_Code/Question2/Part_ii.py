from prepare_data import prepare_data
import numpy as np

Data, Labels = prepare_data()
Label_numbers = np.zeros((11, ))
for label in Labels:
    Label_numbers[label - 1] = Label_numbers[label - 1] + 1

print(Label_numbers)
