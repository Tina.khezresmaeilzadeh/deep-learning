import random

from PIL import Image
import numpy as np


def prepare_data():
    Cropped_Images = []
    Cropped_Image_Labels = []

    for i in range(650):

        # Managing Name
        index = i + 1
        if index < 10:
            name = '00' + str(index)
        elif index < 100:
            name = '0' + str(index)
        else:
            name = str(index)

        # Opening Related .txt File
        my_file = open('NWPU VHR-10 dataset/ground truth/' + str(name) + '.txt', 'r')
        content = my_file.read()
        content_list_complete = content.split("\n")
        content_list = []
        for content_list_element in content_list_complete:
            if content_list_element != '':
                content_list.append(content_list_element)

        my_file.close()

        # Finding Labels
        image_labels = []
        for j in range(len(content_list)):
            txt = content_list[j].split(',')
            txt_rstipped = [x.rstrip(") ") for x in txt]
            final_txt = [int(x.strip("( ")) for x in txt_rstipped]
            image_labels.append(final_txt)

        im = np.array(Image.open('NWPU VHR-10 dataset/positive image set/' + str(name) + '.jpg'), dtype=np.uint8)
        for k in range(len(image_labels)):
            imprime = im[image_labels[k][1]:image_labels[k][3], image_labels[k][0]:image_labels[k][2]]
            imprime = np.array(imprime)
            Cropped_Images.append(imprime)
            Cropped_Image_Labels.append(image_labels[k][4])

    return Cropped_Images, Cropped_Image_Labels


def add_background_class(Data, Labels):
    Cropped_Images = Data
    Cropped_Image_Labels = Labels

    for i in range(150):

        # Managing Name
        index = i + 1
        if index < 10:
            name = '00' + str(index)
        elif index < 100:
            name = '0' + str(index)
        else:
            name = str(index)

        # Opening Related .txt File
        im = np.array(Image.open('NWPU VHR-10 dataset/positive image set/' + str(name) + '.jpg'), dtype=np.uint8)

        row, col, ch = im.shape
        for k in range(6):
            y = random.randint(0, col - 100)

            x = random.randint(0, row - 100)
            imprime = im[x:x + 100, y:y + 100]
            imprime = np.array(imprime)

            Cropped_Images.append(imprime)
            Cropped_Image_Labels.append(11)

    return Cropped_Images, Cropped_Image_Labels
