from prepare_data import prepare_data
import cv2
# Getting Data
import numpy as np

Data, Labels = prepare_data()


def resize_all_data(Data):
    # Resizing All Images to 100*100
    Resized_Data = []
    for image in Data:
        resized_image = cv2.resize(image, (100, 100))
        Resized_Data.append(resized_image)

    return Resized_Data
