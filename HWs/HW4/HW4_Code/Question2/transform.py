import cv2
import numpy as np
from matplotlib import pyplot as plt
import random
from prepare_data import prepare_data, add_background_class
from sklearn.utils import shuffle

random.seed(2)
def rotate_90_clockwise(img):
    img_rotate_90_clockwise = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
    return img_rotate_90_clockwise


def rotate_90_counterclockwise(img):
    img_rotate_90_counterclockwise = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
    return img_rotate_90_counterclockwise


def rotate_180(img):
    img_rotate_180 = cv2.rotate(img, cv2.ROTATE_180)
    return img_rotate_180


def affine_image(img):
    rows, cols, ch = img.shape

    pts1 = np.float32([[50, 50],
                       [200, 50],
                       [50, 200]])

    pts2 = np.float32([[10, 110],
                       [200, 50],
                       [30, 250]])

    M = cv2.getAffineTransform(pts1, pts2)
    dst = cv2.warpAffine(img, M, (cols, rows))
    return dst


def scale_image(img, scale_percent):
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    return resized


def crop_image(img, x, y, h, w):
    crop_img = img[y:y + h, x:x + w]
    return crop_img


def flip_image_vertically(img):
    img_flip_ud = cv2.flip(img, 0)
    return img_flip_ud


def flip_image_horizontally(img):
    img_flip_lr = cv2.flip(img, 1)
    return img_flip_lr


def flip_image_horizontally_and_vertically(img):
    img_flip_ud_lr = cv2.flip(img, -1)
    return img_flip_ud_lr


def add_noise_to_image(noise_typ, image):
    if noise_typ == 0:
        # "gauss"
        row, col, ch = image.shape
        mean = 0
        var = 0.1
        sigma = var ** 0.5
        gauss = np.random.normal(mean, sigma, (row, col, ch))
        gauss = gauss.reshape(row, col, ch)
        noisy = image + gauss
        return noisy
    elif noise_typ == 1:
        # "poisson"
        vals = len(np.unique(image))
        vals = 2 ** np.ceil(np.log2(vals))
        noisy = np.random.poisson(image * vals) / float(vals)
        return noisy
    elif noise_typ == 2:
        # "speckle"
        row, col, ch = image.shape
        gauss = np.random.randn(row, col, ch)
        gauss = gauss.reshape(row, col, ch)
        noisy = image + image * gauss
        return noisy


def do_transform(Data, Labels):
    # Determining Number of Each Label Data
    Label_numbers = np.zeros((11,))
    for label in Labels:
        Label_numbers[label - 1] = Label_numbers[label - 1] + 1

    # Determining Max & Min Number of Label_numbers
    max_length_number = 0
    min_length_number = 0
    for length in Label_numbers:
        if length > max_length_number:
            max_length_number = length
        if length < min_length_number:
            min_length_number = length

    # Threshold of each class length
    threshold = int(0.75*max_length_number)
    New_Data = Data.copy()
    New_Label = Labels.copy()

    # a for loop for each class
    for i in range(len(Label_numbers)):
        Number_of_transforms = 13
        check_not_repetative = np.zeros((len(Data), Number_of_transforms))

        list_of_indices = []
        for j in range(len(Labels)):
            if Labels[j] == i + 1:
                list_of_indices.append(j)

        # adding samples
        counter = Label_numbers[i]

        while counter < Number_of_transforms * Label_numbers[i] and counter < threshold:

            data_index = random.choice(list_of_indices)

            transform_index = random.randint(0, Number_of_transforms - 1)

            if check_not_repetative[data_index][transform_index] == 0:
                counter = counter + 1
                check_not_repetative[data_index][transform_index] = 1

                if transform_index == 0:
                    transformed = rotate_90_clockwise(Data[data_index])

                elif transform_index == 1:
                    transformed = rotate_90_counterclockwise(Data[data_index])

                elif transform_index == 2:
                    transformed = rotate_180(Data[data_index])

                elif transform_index == 3:
                    transformed = affine_image(Data[data_index])

                elif transform_index == 4:
                    transformed = scale_image(Data[data_index], 30)

                elif transform_index == 5:
                    transformed = scale_image(Data[data_index], 50)

                elif transform_index == 6:
                    transformed = scale_image(Data[data_index], 70)

                elif transform_index == 7:
                    transformed = flip_image_vertically(Data[data_index])

                elif transform_index == 8:
                    transformed = flip_image_horizontally(Data[data_index])

                elif transform_index == 9:
                    transformed = flip_image_horizontally_and_vertically(Data[data_index])

                elif transform_index == 10:
                    transformed = add_noise_to_image(0, Data[data_index])

                elif transform_index == 11:
                    transformed = add_noise_to_image(1, Data[data_index])

                else:
                    transformed = add_noise_to_image(2, Data[data_index])
                transformed = np.array(transformed)
                New_Data.append(transformed)
                New_Label.append(Labels[data_index])

    return New_Data, New_Label


