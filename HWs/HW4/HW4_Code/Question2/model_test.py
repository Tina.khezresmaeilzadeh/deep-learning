import torch
import cv2
from net import CNN
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt
import matplotlib.patches as patches
from nms import non_max_suppression, soft_nms_pytorch

# Loading the Model
model = CNN()
model.load_state_dict(torch.load('model.ckpt'))

# Please set the state to "nms" or "snms" or "ordinary"
state = 'ordinary'

# Loading the Desired Image
im = np.array(Image.open('NWPU VHR-10 dataset/positive image set/200.jpg'), dtype=np.uint8)

# Setting up The device
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
model = model.to(device)

# Printing the Shape of the Image
row, col, ch = im.shape
print('shape of image:', im.shape)

# Pyramid Gaussian; We have a list named gpA that consists of the image and its pyramids.
G = im.copy()
gpA = [G]
for i in range(1):
    G = cv2.pyrDown(G)
    gpA.append(G)
    print(G.shape)

# For each image, we crop images with size 100*100*3 and predict their label, the overlap of images is 60 ( the step
# is 40 )
# The Labels is a list with elements like this: [Label, Probability Score, x1, y1, x2, y2]
for image in gpA:
    Labels = []
    Labels_for_SNMS = []
    Scores_for_SNMS = []
    Class_Names_for_SNMS = []
    for index_x in range(0, image.shape[0] - 101, 40):
        for index_y in range(0, image.shape[1] - 101, 40):
            Cropped_Image = image[index_x:index_x + 100, index_y:index_y + 100]
            Cropped_Image = torch.from_numpy(Cropped_Image)
            Cropped_Image = Cropped_Image / 255
            Cropped_Image = torch.reshape(Cropped_Image, (1, 3, 100, 100))
            images = Cropped_Image.to(device)
            outputs = model(images.float())
            _, predicted = torch.max(outputs.data, 1)
            index_x_real1 = int(index_x / (image.shape[0] / gpA[0].shape[0]))
            index_y_real1 = int(index_y / (image.shape[1] / gpA[0].shape[1]))
            index_x_real2 = int((index_x + 100) / (image.shape[0] / gpA[0].shape[0]))
            index_y_real2 = int((index_y + 100) / (image.shape[1] / gpA[0].shape[1]))
            if int(predicted + 1) != 11 and index_x_real2 < row and index_y_real2 < col:
                Labels.append(
                    [int(predicted) + 1, float(outputs[0][predicted]), index_x_real1, index_y_real1, index_x_real2,
                     index_y_real2])
                # These lists are just inputs to snms function because it gets bboxes in this form: [y1, x1, y2, x2]
                # and also thee scores list seperately
                Labels_for_SNMS.append([index_y_real1, index_x_real1, index_y_real2, index_x_real2])
                Scores_for_SNMS.append(float(outputs[0][predicted]))
                Class_Names_for_SNMS.append(int(predicted) + 1)

# The output is the sorted bboxes with respect to their updated scores and also remove the ones not obeying
# the threshold limits.
Final_Class_Indices = soft_nms_pytorch(torch.from_numpy(np.array(Labels_for_SNMS)),
                                       torch.from_numpy(np.array(Scores_for_SNMS)),
                                       sigma=0.5, thresh=0.7, cuda=0)

Final_Labels = []
# If we are in "snms" state, we should sort the bboxes in the order obtained
# from soft_nms_pytorch function in nms.py file
if state == 'snms':
    Final_Labels = []
    for i in Final_Class_Indices[0]:
        Final_Labels.append(Labels[int(i)])
else:
    # In "nms" mode, we use non_max_suppression function from nms.py. It handles suitable bboxes itself.
    Final_Labels = non_max_suppression(bboxes=Labels, iou_threshold=0.05, threshold=0.6, box_format="corners")
    print('length: ', len(Final_Labels))

fig, ax = plt.subplots(1)
print(im.shape)
ax.imshow(im)

# In this case we don't need nms or snms functions.
if state == "ordinary":
    Final_Labels = Labels

# Drawing rectangles
for i in range(len(Final_Labels)):
    rect = patches.Rectangle((Final_Labels[i][3], Final_Labels[i][2]), Final_Labels[i][5] - Final_Labels[i][3],
                             Final_Labels[i][4] - Final_Labels[i][2],
                             linewidth=2, edgecolor='g', facecolor='none')

    ax.add_patch(rect)

# Adding Labels
for i in range(len(Final_Labels)):
    centerx = int((Final_Labels[i][3] + Final_Labels[i][5]) / 2 - 10)
    centery = int((Final_Labels[i][2] + Final_Labels[i][4]) / 2 + 10)

    plt.text(centerx, centery, str(Final_Labels[i][0]), color='r')

plt.show()
