# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
from prepare_data import prepare_data
from visual_data import visualize
from transform import do_transform
import numpy as np
from prepare_data import add_background_class
from preprocess_data import resize_all_data


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    visualize('628')




# See PyCharm help at https://www.jetbrains.com/help/pycharm/
