﻿## HW4

This homework is about implementation of following networks:

 - Implementing a music composer using RNN
 - Implementing a sattelite object detection using a CNN network on **NWPU VHR­-10** dataset with data augmentation such as different transformations and rotations and using pyramid gaussian and non-ⅿaxiⅿuⅿ-suppression(nⅿs) for prediction part.
