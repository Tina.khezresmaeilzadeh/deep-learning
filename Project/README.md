﻿## Project

The aim of this project is to classify cassava leaves and distiguish their diseases of [kaggle website competition named cassava leaf disease classification](https://www.kaggle.com/c/cassava-leaf-disease-classification)using data augmentation and a special network structure for submitting in kaggle competition. Furthermore, it contains creating images which are completely like cassava leaves using conditional GAN and conditional VAE and evaluating GQI index (explained in the article) for generated images.
 
